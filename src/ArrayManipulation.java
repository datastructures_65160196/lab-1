import java.util.Arrays;

public class ArrayManipulation {
    public static void main(String[] args) {
        int[] numbers = {5,8,3,2,7};
        String[] names = {"Alice", "Bob", "Charlie", "David"};
        double[] values = new double[4];

        //3.Print the elements of the "numbers" array using a for loop.
        for(int i=0; i<numbers.length; i++) {
            System.out.print(numbers[i]+ " ");
        }

        System.out.println();

        //4.Print the elements of the "names" array using a for-each loop.
        for(String name : names) {
            System.out.print(name+" ");
        }

        System.out.println();

        //5.Initialize the elements of the "values" array with any four decimal values of your choice.
        values[0] = 2.53;
        values[1] = 5.79;
        values[2] = 8.62;
        values[3] = 7.34;
        
        //6.Calculate and print the sum of all elements in the "numbers" array.
        int sum = 0;
        for(int i=0; i<numbers.length; i++) {
            sum = numbers[i]+sum;
        }
        System.out.println(sum);
        
        //7.Find and print the maximum value in the "values" array.
        double max = values[0];
        for(int i=1; i < values.length; i++) {
            if(values[i] > max) {
                max = values[i];
            }
        }
        System.out.println(max);

        //8.Create a new string array named "reversedNames" with the same length as the "names" array.
        String[] reversedNames = new String[names.length];

            //Fill the "reversedNames" array with the elements of the "names" array in reverse order.
            int n = reversedNames.length;
            for (int i = 0; i < n / 2; i++) {
                String temp = names[i];
                reversedNames[i] = names[n - 1 - i];
                reversedNames[n - 1 - i] = temp;
            }

            //Print the elements of the "reversedNames" array.
            for (String rename : reversedNames) {
                System.out.print(rename + " ");
            }

        System.out.println();

        //9.BONUS: Sort the "numbers" array in ascending order using any sorting algorithm of your choice.
        Arrays.sort(numbers);
            
            //Print the sorted "numbers" array.
            for(int sortnum : numbers) {
                System.out.print(sortnum+" ");
            }
    }
}